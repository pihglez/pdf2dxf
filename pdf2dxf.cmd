@echo off
:: Thanks to Steve Jansen for making this possible
:: http://steve-jansen.github.io/guides/windows-batch-scripting/index.html

SETLOCAL ENABLEEXTENSIONS
:: Setting Script main variables
set app_name=PDF2DXF batch processor v.1.1 by @pihglez (2021)
set parent_folder=%~dp0
set dxf_script_loc="C:\Program Files\Inkscape\share\inkscape\extensions\dxf_outlines.py"
set pdf2svg_exe="%parent_folder%\pdf2svg.exe"
set str_keep_svg=-k


:: Setting serveral errorlevels
set /a ERROR_PDF2SVG=1
set /a ERROR_DXF_OUTLINES=2
set /a ERROR_FILE_NOT_FOUND=3

:: Counting Number of arguments
set /a count=0
for %%I in (%*) do (
	set /a count+=1
)

:: Check all pre-requisites. Not so elegant but works ;)
:: pdf2svg.exe does exist
if not exist %pdf2svg_exe% (
	ECHO %~n0: Pre-requisites not met: file not found - %pdf2svg_exe% >&2
	EXIT /B %ERROR_PDF2SVG%
)

:: The dxf_outlines.py script does exist
if not exist %dxf_script_loc% (
	ECHO %~n0: Pre-requisites not met: InkScape python script not found - %dxf_script_loc% >&2
	EXIT /B %ERROR_DXF_OUTLINES%
)

:: If no file is provided, show help
if %count% EQU 0 goto show_help

:: The file to be processed exists
if not exist %1 (
	ECHO %~n0: The file to be processed must exist - '%1' >&2
	EXIT /B %ERROR_FILE_NOT_FOUND%
)


:: Setting variables
set original_file=%~1
set keep_svg_file=%~2
set original_name=%~n1
set svg_file=%original_name%.svg
set dxf_file=%original_name%.dxf
set cur_folder=%cd%


echo.
echo %app_name%
echo.
echo     Application folder: "%parent_folder%"
echo     Current folder:     "%cur_folder%"

echo.

echo Processing "%original_file%"
echo     Trying to save to "%svg_file%"

:: Run PDF > SVG conversion
CALL %pdf2svg_exe% "%original_file%" "%svg_file%"

if not exist "%svg_file%" (
	ECHO.
	ECHO %~n0: The conversion to '%svg_file%' didn't succeed. Stopping process. Contact @pihglez for support. >&2
	EXIT /B %ERROR_FILE_NOT_FOUND%
)
echo     Done.


:: Run Python Script under InkScape installation folder
:: to convert SVG > DXF
echo     Trying to save to "%dxf_file%"
CALL python %dxf_script_loc% --output="%dxf_file%" "%svg_file%"
echo     Done.

:: Removes .svg transitional file
if %count% GEQ 2 (
	echo Should check if arg is '-k' to jump to show_help
	REM if /I %2 EQ %str_keep_svg% goto good_bye
)
echo     Cleaning...
del "%svg_file%"
echo     Done.
echo.

:good_bye
echo Drop me a mail to pihglez@gmail.com if you find this piece of code useful.
echo Enjoy! :)
echo.

:: force execution to quit at the end of the main logic
EXIT /B 0


:show_help
set app_help1=Usage:  pdf2dxf.cmd pdf_file_name [%str_keep_svg%] (Not Implemented)
set app_help2=i.e.:   pdf2dxf.cmd "my special pdf.pdf"
set app_help3=        Option %str_keep_svg% keeps transitional .svg file
set app_help4=i.e.:   pdf2dxf.cmd "my special pdf.pdf" %str_keep_svg% (to keep transational .svg file)
echo.
echo %app_name%
echo.
echo %app_help1%
echo %app_help3%
echo.
echo %app_help2%
echo %app_help4%
EXIT /B 0